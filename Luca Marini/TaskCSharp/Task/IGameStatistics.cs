namespace Task
{
    /// <summary>
    /// It stores the data of the game.
    /// </summary>
    public interface IGameStatistics
    {
        /// <summary>
        /// Gets and sets the last level completed.
        /// </summary>
        int CurrentLevel { get; set; }
    }
}