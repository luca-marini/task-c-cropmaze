namespace Task
{
    /// <summary>
    /// Interface that maintains the statistics of various entities.
    /// </summary>
    public interface IEntityStatistics
    {
        /// <summary>
        /// Returns the Health Points of the Player.
        /// </summary>
        int PlayerHealth { get; }

        /// <summary>
        /// Gets the Health Points of the enemy, based on the level number.
        /// </summary>
        /// <param name="currentLevel">The number of the current level</param>
        /// <returns>the Health Points of the enemy</returns>
        int GetEnemyHealth(int currentLevel);

    }
}