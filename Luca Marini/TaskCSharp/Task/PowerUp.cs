using System.Drawing;

namespace Task
{
    public class PowerUp : IEntity
    {
        public PowerUp(BodyBuilder bodyBuilder, PointF position, TemporaryDoubleSpeed temporaryDoubleSpeed)
        {
            
        }

        public PowerUp(BodyBuilder bodyBuilder, PointF position, TemporaryDoubleDamage temporaryDoubleSpeed)
        {
            
        }

        public PowerUp(BodyBuilder bodyBuilder, PointF position, TemporaryShield temporaryDoubleSpeed)
        {
            
        }

        public void Update(double up)
        {
            
        }

        public void Destroy()
        {
            
        }
    }
}