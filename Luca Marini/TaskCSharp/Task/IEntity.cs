using System.Data;

namespace Task
{
    public interface IEntity
    {
        void Update(double up);

        void Destroy();
    }
}