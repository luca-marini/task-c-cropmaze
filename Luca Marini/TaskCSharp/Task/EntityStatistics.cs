namespace Task
{
    public sealed class EntityStatistics : IEntityStatistics
    {
        public EntityStatistics()
        {
            PlayerHealth = PlayerHealthPoints;
            EnemyHealth = EnemyHealthPoints;
        }

        private const int PlayerHealthPoints = 10_000;
        private const int EnemyHealthPoints = 10;
        
        public int PlayerHealth { get; }
        public int GetEnemyHealth(int currentLevel)
        {
            return EnemyHealth * currentLevel;
        }

        private int EnemyHealth { get; }
    }
}