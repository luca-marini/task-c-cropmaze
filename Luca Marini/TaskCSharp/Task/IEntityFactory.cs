using System.Drawing;

namespace Task
{
    /// <summary>
    /// Abstract Factory for the entities.
    /// </summary>
    public interface IEntityFactory
    { 
         /// <summary>
         /// Creates the Player.
         /// </summary>
         /// <param name="position">Position where the player spawn</param>
         /// <returns>the Player</returns>
         Player CreatePlayer(PointF position);

         /// <summary>
         /// Creates an Alien.
         /// </summary>
         /// <param name="position">Position where the enemy spawn</param>
         /// <returns>an enemy (alien)</returns>
         Alien CreateEnemy(PointF position); 
         
         /// <summary>
         /// Creates a Coin.
         /// </summary>
         /// <param name="position">Position where the coin spawns</param>
         /// <returns>a coin</returns>
         Coin CreateCoin(PointF position);

         
         /// <summary>
         /// Creates a Wall.
         /// </summary>
         /// <param name="position">Position where the wall spawns</param>
         /// <param name="dimension">Dimension of the wall</param>
         /// <returns></returns>
         Wall  CreateWall(PointF position, PointF dimension);

         /// <summary>
         /// Creates a TemporaryDoubleSpeed power up
         /// </summary>
         /// <param name="position">Position where the double speed power up spawn</param>
         /// <returns>a TemporaryDoubleSpeed power up</returns>
         PowerUp CreateDoubleSpeed(PointF position);
   
         /// <summary>
         /// Creates a TemporaryDoubleDamage power up
         /// </summary>
         /// <param name="position">Position where the double damage power up spawn</param>
         /// <returns>a TemporaryDoubleDamage power up</returns>
         PowerUp CreateDoubleDamage(PointF position);

         /// <summary>
         /// Creates a TemporaryShield power up
         /// </summary>
         /// <param name="position">Position where the shield power up spawn</param>
         /// <returns>a TemporaryShield power up</returns>
         PowerUp CreateShield(PointF position);
    }

}
