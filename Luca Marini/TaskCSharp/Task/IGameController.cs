using System.Diagnostics;

namespace Task
{
    public interface IGameController
    {
        void Stop();

        void TriggerEndLevel();
        
        void TriggerGameOver();

    }
}