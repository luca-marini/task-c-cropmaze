namespace Task
{
    public sealed class BasicScoreCalculator : IScoreCalculator
    {
        private const int KillMultiplier = 50;
        private const int MoneyMultiplier = 1;

        public int CalculateScore(IPlayerStatistics playerStatistics)
        {
            return playerStatistics.CollectedMoney * MoneyMultiplier + playerStatistics.KilledEnemies * KillMultiplier;
        }
    }
}