using System.Collections.Generic;

namespace Task
{
    /// <summary>
    /// Runs the game field.
    /// </summary>
    public interface IGameField
    {
        /// <summary>
        /// Update the field simulation.
        /// </summary>
        /// <param name="up">Time to simulate, in seconds</param>
        void Update(double up);
        
        /// <summary>
        /// Adds an entity in the game field.
        /// </summary>
        /// <param name="entity">The entity that is going to be added in the game field</param>
        /// <returns>the entity</returns>
        IEntity addEntity(IEntity entity);

        /// <summary>
        /// Gets all the entities in the game.
        /// </summary>
        ISet<IEntity> Entities {get;}

        /// <summary>
        /// Removes the entity passed by argument.
        /// </summary>
        /// <param name="entity">The entity to remove</param>
        void RemoveEntity(IEntity entity);

        /// <summary>
        /// Gets all the walls present in the game field
        /// </summary>
        /// <returns>the walls</returns>
        ISet<IEntity> GetWalls();
        
        /// <summary>
        /// Destroy an entity in the field.
        /// </summary>
        /// <param name="deathEvent">Event that triggers the destruction</param>
       void DestroyEntity(IDeathEvent deathEvent);

        /// <summary>
        /// Getter for the player's statistics.
        /// </summary>
       IPlayerStatistics PlayerStatistics {get;}
    }
}