namespace Task
{
    /// <summary>
    /// Calculates the score.
    /// </summary>
    public interface IScoreCalculator
    {
        /// <summary>
        /// Gets the calculated score.
        /// </summary>
        /// <param name="playerStatistics">stores the Player's statistics, like the money collected and the killed enemies</param>
        /// <returns>the score</returns>
        int CalculateScore(IPlayerStatistics playerStatistics);
        
    }
}