namespace Task
{
    public sealed class PlayerStatistics : IPlayerStatistics
    {
        private const int Increase = 1;

        public PlayerStatistics() {
            KilledEnemies = 0;
            CollectedMoney = 0;
            CollectedMoney = CollectedMoney;
            KilledEnemies = KilledEnemies;
        }

        public void IncreaseCollectedMoney()
        {
            CollectedMoney += Increase;
        }

        public void IncreaseKilledEnemies()
        {
            KilledEnemies += Increase;
        }

        public int CollectedMoney { get; private set; }

        public int KilledEnemies { get; private set; }
    }
}