namespace Task
{
    public interface IDeathEvent
    {
        IEntity GetEntity();
    }
}