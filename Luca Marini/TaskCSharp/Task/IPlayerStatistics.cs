namespace Task
{
    /// <summary>
    /// Stores the Player statistics.
    /// </summary>
    public interface IPlayerStatistics
    {
        /// <summary>
        /// Increases the number of collected money.
        /// </summary>
        void IncreaseCollectedMoney();
        
        /// <summary>
        /// Increases the number of killed enemies.
        /// </summary>
        void IncreaseKilledEnemies();

        /// <summary>
        /// Gets the amount of collected money, that is the value of the all collected coins.
        /// </summary>
        int CollectedMoney { get; }

        /// <summary>
        /// Gets the amount of killed enemies.
        /// </summary>
        int KilledEnemies { get; }
    }
}