using System;
using System.Drawing;

namespace Task
{
    /// <summary>
    /// Factory implementation of the AbstractFactory EntityFactory.
    /// </summary>
    public class EntityFactory : IEntityFactory
    {
        private static readonly double Timeout = 10;

        private readonly IGameField _gameField;
        private readonly IEntityStatistics _entityStatistics;
        private readonly IGameStatistics _gameStatistics;
        private Player _player;
        
        public EntityFactory(IGameField gameField, IEntityStatistics entityStatistics, IGameStatistics gameStatistics) {
            _gameField = gameField;
            _entityStatistics = entityStatistics;
            _gameStatistics = gameStatistics;
        }
        
        public Player CreatePlayer(PointF position)
        {
            _player = new Player(new BodyBuilder(), position, _entityStatistics.PlayerHealth, _gameField);
            return _player;
        }

        public Alien CreateEnemy(PointF position)
        {
            if (_player == null)
            {
                throw new ArgumentException("Error: The Player is not present");
            }
            return new Alien(new BodyBuilder(), position, _entityStatistics.GetEnemyHealth(_gameStatistics.CurrentLevel), _gameField.GetWalls(), _player);
        }

        public Coin CreateCoin(PointF position)
        {
            return new Coin(new BodyBuilder(), position);
        }

        public Wall CreateWall(PointF position, PointF dimension)
        {
            return new Wall(new BodyBuilder(), position, dimension);
        }

        public PowerUp CreateDoubleSpeed(PointF position)
        {
            return new PowerUp(new BodyBuilder(), position, new TemporaryDoubleSpeed(Timeout));
        }

        public PowerUp CreateDoubleDamage(PointF position)
        {
            return new PowerUp(new BodyBuilder(), position, new TemporaryDoubleDamage(Timeout));
        }

        public PowerUp CreateShield(PointF position)
        {
            return new PowerUp(new BodyBuilder(), position, new TemporaryShield(Timeout));
        }
    }
}