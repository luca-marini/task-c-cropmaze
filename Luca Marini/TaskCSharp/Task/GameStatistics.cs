namespace Task
{
    /// <summary>
    /// Implementation of GameStatistics.
    /// </summary>
    public class GameStatistics : IGameStatistics
    {
        private static readonly int FirstLevel = 1;

        public GameStatistics()
        {
            CurrentLevel = FirstLevel;
        }

        public int CurrentLevel { get; set; }
    }
}