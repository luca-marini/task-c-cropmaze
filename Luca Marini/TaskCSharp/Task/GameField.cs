using System;
using System.Collections.Generic;
using System.Linq;

namespace Task
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GameField : IGameField
    {
        private readonly ISet<IEntity> _entities;
        private readonly ICollisionSupervisor _collisionSupervisor;
        private readonly ISet<IEntity> _entitiesToBeRemoved;
        private readonly IGameController _gameController;
        private readonly IPlayerStatistics _playerStatistics;
        
        public GameField(ICollisionSupervisor collisionSupervisor, IGameController gameController)
        {
            PlayerStatistics = _playerStatistics;
            _entities = new HashSet<IEntity>();
            Entities = _entities;
            _collisionSupervisor = collisionSupervisor;
            _entitiesToBeRemoved = new HashSet<IEntity>();
            _gameController = gameController;
            _playerStatistics = new PlayerStatistics();
        }
        
        public void Update(double up)
        {
            //Updates all the entities
            ManageEntitiesToBeRemoved();
            UpdateEntities(up);
            //Searches for collisions
            _collisionSupervisor.SearchCollision();
            //If there are no enemies (aliens) it removes all the entities except the Player and it goes to the next level
            if (AreAllEnemiesDead()) {
                _entities.AsEnumerable()
                        .Where(e => !(e.GetType() == typeof(Player)))
                        .ToList()
                        .ForEach(e => _entitiesToBeRemoved.Add(e));
                ManageEntitiesToBeRemoved();
                if (_gameController == null)
                {
                    throw new NullReferenceException();
                }
                _gameController.Stop();
                _gameController.TriggerEndLevel();
            }
            //If there is not a Player, he's dead and it is a Game Over
            if (IsPlayerDead()) {
                if (_gameController == null)
                {
                    throw new NullReferenceException();
                } 
                _gameController.Stop();
                _gameController.TriggerGameOver();
            }
        }
        
        public IEntity addEntity(IEntity entity)
        {
            _entities.Add(entity);
           // if (entity.get(Collidable.class).isPresent()) {
           //     this.collisionSupervisor.addCollisionComponent(entity.get(Collidable.class).get());
            //}
            //entity.getDeathEvent().register(this::destroyEntity);
            return entity;
        }

        public ISet<IEntity> Entities { get; }

        public void RemoveEntity(IEntity entity)
        {
            //if (entity.get(Collidable.class).isPresent()) {
            //    this.collisionSupervisor.removeCollisionComponent(entity.get(Collidable.class).get());
            //}
            _entities.Remove(entity);
            entity.Destroy();
        }
        
        public ISet<IEntity> GetWalls()
        {
            return _entities.AsEnumerable().Where(entity => entity is Wall).ToHashSet();
        }
        
        public void DestroyEntity(IDeathEvent deathEvent)
        {
            //if an Alien is killed, it controls the number of alive Aliens, and if it is one (the last Alien killed),
            // then the next level has to start
            IEntity entity = deathEvent.GetEntity();
            if (entity is Alien)
            {
                _playerStatistics.IncreaseKilledEnemies();
            }
            if (entity is Coin) {
                _playerStatistics.IncreaseCollectedMoney();
            }
            _entitiesToBeRemoved.Add(entity);
        }

        public IPlayerStatistics PlayerStatistics { get; }
        
        private bool IsPlayerDead()
        {
            return  !_entities.AsEnumerable().Any(e => e is Player);
        }
        
        private bool AreAllEnemiesDead()
        {
            return !_entities.AsEnumerable().Any(e => e is Alien);
        }
        
        private void ManageEntitiesToBeRemoved() {
            //entitiesToBeRemoved.forEach(e -> e.getDeathEvent().deregister(this::destroyEntity));
            _entitiesToBeRemoved.ToList().ForEach(RemoveEntity);
            _entitiesToBeRemoved.Clear();
        }
        
        private void UpdateEntities(double up) {
            _entities.AsEnumerable()
                    .Where(e => !_entitiesToBeRemoved.Contains(e))
                    .ToList()
                    .ForEach(e => e.Update(up));
        }
    }
}