namespace Task
{
    public interface ICollisionSupervisor
    {
        void SearchCollision();
    }
}