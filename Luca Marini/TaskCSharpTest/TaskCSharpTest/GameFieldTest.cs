using System;
using System.Drawing;
using NUnit.Framework;
using Task;

namespace TaskCSharpTest
{
    [TestFixture]
    public class GameFieldTest
    {

        private IEntityStatistics _entityStatistics;
        private IGameStatistics _gameStatistics;
        private IGameField _gameField;
        private IEntityFactory _entityFactory;

        [SetUp]
        public void FixtureSetup()
        {
            _entityStatistics = new EntityStatistics();
            _gameStatistics = new GameStatistics();
            _gameField = new GameField(new CollisionSupervisor(), new GameController());
            _entityFactory = new EntityFactory(_gameField, _entityStatistics, _gameStatistics);
        }

        [Test]
        public void TestEntityFactoryAndGameField()
        {
            //Test on EntityFactory

            //Player creation
            Player player1 = _entityFactory.CreatePlayer(new PointF((float) 2.5, (float) 2.5));
            Assert.AreEqual(player1.GetType(), typeof(Player));

            //Wall creation
            PointF positionWall1 = new PointF(1, 1);
            Wall wall1 = _entityFactory.CreateWall(positionWall1, new PointF(5, 5));
            Assert.AreEqual(wall1.GetType(), typeof(Wall));

            //Enemy creation
            int levelNumber = 5;
            _gameStatistics.CurrentLevel = levelNumber;
            Alien alien1 = _entityFactory.CreateEnemy(new PointF(12, 12));
            Assert.AreEqual(alien1.GetType(), typeof(Alien));

            //Coin creation
            Coin coin1 = _entityFactory.CreateCoin(new PointF((float) 4.4, (float) 4.4));
            Assert.AreEqual(coin1.GetType(), typeof(Coin));
            
            
            //Test on GameField
            Assert.IsTrue(_gameField.Entities.Count == 0);
            _gameField.addEntity(wall1);
            Assert.AreEqual(_gameField.Entities.Count, 1);
            _gameField.addEntity(player1);
            _gameField.addEntity(_entityFactory.CreateWall(new PointF(2, 3), new PointF(6, 8)));
            _gameField.addEntity(coin1);
            Assert.AreEqual(_gameField.GetWalls().Count, 2);

            _gameField.RemoveEntity(wall1);
            Assert.IsFalse(_gameField.Entities.Contains(wall1));
            Assert.AreEqual(_gameField.GetWalls().Count, 1);
            Assert.AreEqual(_gameField.Entities.Count, 3);
        }

        [Test]
        public void TestInitializeAlienBeforePlayer()
        {
            //Enemy creation
            Assert.Throws<ArgumentException>(() => _entityFactory.CreateEnemy(new PointF(12, 12)));

            //Player creation
            _entityFactory.CreatePlayer(new PointF((float) 2.5, (float) 2.5));
        }
    }
}