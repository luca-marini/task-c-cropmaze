using System.Linq;
using NUnit.Framework;
using Task;

namespace TaskCSharpTest
{
    [TestFixture]
    public class ScoreCalculatorTest
    {
        private IScoreCalculator _scoreCalculator;
        private IPlayerStatistics _playerStatistics;
        
        [SetUp]
        public void FixtureSetup() 
        {
            _scoreCalculator= new BasicScoreCalculator();
            _playerStatistics = new PlayerStatistics();
        }
        
        [Test]
        public void TestCoinCollection() {
            Enumerable.Range(0,5).ToList().ForEach(i => _playerStatistics.IncreaseCollectedMoney());
            Assert.AreEqual(_playerStatistics.CollectedMoney, 5);
        }

        [Test]
        public void CalculateScoreWithOnlyMoney() {
            Enumerable.Range(0,12).ToList().ForEach(i => _playerStatistics.IncreaseCollectedMoney());
            Assert.AreEqual(_scoreCalculator.CalculateScore(_playerStatistics), 12);
        }

        [Test]
        public void TestKillingEnemies() {
            Enumerable.Range(0,10).ToList().ForEach(i => _playerStatistics.IncreaseKilledEnemies());
            Assert.AreEqual(_playerStatistics.KilledEnemies, 10);
        }

        [Test]
        public void CalculateScoreWithOnlyKilledEnemies() {
            Enumerable.Range(0,10).ToList().ForEach(i => _playerStatistics.IncreaseKilledEnemies());
            Assert.AreEqual(_scoreCalculator.CalculateScore(_playerStatistics), 500);
        }

        [Test]
        public void CalculateScoreWithCoinCollectionAndKilledEnemies() {
            Enumerable.Range(0,120).ToList().ForEach(i => _playerStatistics.IncreaseCollectedMoney());
            Enumerable.Range(0,10).ToList().ForEach(i => _playerStatistics.IncreaseKilledEnemies());
            Assert.AreEqual(_scoreCalculator.CalculateScore(_playerStatistics), 620);
        }
    }
}