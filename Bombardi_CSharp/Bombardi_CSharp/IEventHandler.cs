﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public interface IEventHandler<T>
    {
        void Handle(T argument);
    }
}
