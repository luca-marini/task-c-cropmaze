﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public class CollisionSupervisorImpl : ICollisionSupervisor
    {
        private readonly List<ICollidable> collidableComponents;
        private readonly CollisionLabel collisionLabel = new CollisionLabel();

        public CollisionSupervisorImpl()
        {
            this.collidableComponents = new List<ICollidable>();
        }
        public void AddCollisionComponent(ICollidable collidableComponent)
        {
            this.collidableComponents.Add(collidableComponent);
        }

        public void RemoveCollisionComponent(ICollidable collidableComponent)
        {
            this.collidableComponents.Remove(collidableComponent);
        }

        public void SearchCollision()
        {
            for(int i = 0; i < this.collidableComponents.Count; i++)
            {
                ICollidable c1 = collidableComponents.ElementAt(i);
                for(int j=0; j<this.collidableComponents.Count; j++)
                {
                    ICollidable c2 = collidableComponents.ElementAt(j);
                    VerifyCollision(c1, c2);
                }
            }
        }

        private void VerifyCollision(ICollidable c1, ICollidable c2)
        {
            if (this.collisionLabel.CanCollideWith(c1.GetLabel, c2.GetLabel)&&c1.GetShape.IntersectsWith(c2.GetShape))
            {
                NotifyCollision(c1, c2, CollisionLabel.Label.COIN);
                NotifyCollision(c1, c2, CollisionLabel.Label.SHOT);
                NotifyCollision(c1, c2, CollisionLabel.Label.POWER);
                NotifyCollision(c1, c2, CollisionLabel.Label.ALIEN);
            }
        }

        private void NotifyCollision(ICollidable c1, ICollidable c2, CollisionLabel.Label label)
        {
            if(c1.GetLabel.Equals(label) || c2.GetLabel.Equals(label))
            {
                if (c1.GetLabel.Equals(label))
                {
                    c1.NotifyCollision(new Collision(c2));
                }
                else
                {
                    c2.NotifyCollision(new Collision(c1));
                }
            }
            
        }
    }
}
