﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public interface IEvent<T>
    {
        void Register(Action<T> eventHandler);

        void Deregister(Action<T> eventHandler);
    }
}
