﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Bombardi_CSharp
{
    public class CollisionLabel
    {
        public enum Label { PLAYER, ALIEN, SHOT, POWER, COIN, WALL };

        public Boolean CanCollideWith(Label mylabel, Label other)
        {
            switch (mylabel)
            {
                case Label.PLAYER:
                    return other == Label.ALIEN || other == Label.POWER || other == Label.COIN;
                case Label.ALIEN:
                    return other == Label.SHOT;
                case Label.SHOT:
                    return other == Label.ALIEN || other == Label.WALL;
                case Label.POWER:
                    return other == Label.PLAYER;
                case Label.COIN:
                    return other == Label.PLAYER;
                case Label.WALL:
                    return  other == Label.SHOT;
                default:
                    return false;
            }
        }
    }

}
