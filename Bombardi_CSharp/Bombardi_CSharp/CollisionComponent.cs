﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public class CollisionComponent : ICollidable
    {
        private readonly RectangleF shape;
        private readonly CollisionLabel.Label label;
        private readonly EventSource<Collision> collisionEvent;

        public CollisionComponent(RectangleF shape, CollisionLabel.Label label)
        {
            this.shape = shape;
            this.label = label;
            this.collisionEvent = new EventSource<Collision>();
        }

        public RectangleF GetShape
        {
            get { return shape; }
        }

        public CollisionLabel.Label GetLabel
        {
            get { return label; }
        }

        public IEvent<Collision> GetEvent
        {
            get { return collisionEvent; }
        }

        public void NotifyCollision(Collision collision)
        {
            this.collisionEvent.Trigger(collision);
        }
    }
}
