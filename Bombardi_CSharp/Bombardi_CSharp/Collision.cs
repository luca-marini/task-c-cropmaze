﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public class Collision
    {
        private readonly ICollidable obj;

        public Collision(ICollidable other)
        {
            this.obj = other;
        }
     
        public ICollidable GetCollisionComponent()
        {
            return obj;
        }
    }
}
