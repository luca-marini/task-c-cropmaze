﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Bombardi_CSharp
{
    public class EventSource<T> : IEvent<T>
    {
        private readonly ConcurrentQueue<Action<T>> registeredHandlers;

        public EventSource()
        {
            this.registeredHandlers = new ConcurrentQueue<Action<T>>();
        }

        public void Deregister(Action<T> eventHandler)
        {
            this.registeredHandlers.TryDequeue(out eventHandler);
        }

        public void Register(Action<T> eventHandler)
        {
            this.registeredHandlers.Enqueue(eventHandler);
        }
    
        public void Trigger(T argument)
        {
                this.registeredHandlers.ToList().ForEach(x => x.DynamicInvoke(argument));       
        }
    }
}
