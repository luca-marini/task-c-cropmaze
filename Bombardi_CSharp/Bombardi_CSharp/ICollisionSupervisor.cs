﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombardi_CSharp
{
    public interface ICollisionSupervisor
    {
        void SearchCollision();

        void AddCollisionComponent(ICollidable collidableComponent);

        void RemoveCollisionComponent(ICollidable collidableComponent);
    }
}
