﻿using System.Windows;
using System.Drawing;
namespace Bombardi_CSharp
{
    public interface ICollidable
    {
        RectangleF GetShape{ get; }

        CollisionLabel.Label GetLabel { get; }

        IEvent<Collision> GetEvent { get; }

        void NotifyCollision(Collision collision);
    }
}
