﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bombardi_CSharp;
using System.Drawing;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        private ICollisionSupervisor collisionSupervisor;
        private bool result = false;

        [TestInitialize]
        public void SetUp()
        {
            this.collisionSupervisor = new CollisionSupervisorImpl();
        }

        [TestMethod]
        public void TestMethod1()
        {
            RectangleF rectangleTall = new RectangleF(0, 0, 1, 2);
            RectangleF rectangleLarge = new RectangleF(0, 0, 2, 1);
            RectangleF square = new RectangleF(10, 5, 1, 1);
            this.CheckCollision(rectangleTall, rectangleLarge, true);
            this.CheckCollision(rectangleTall, square, false);
        }

        private void CheckCollision(RectangleF r1, RectangleF r2, bool expected)
        {
            ICollidable coll1 = new CollisionComponent(r1, CollisionLabel.Label.COIN);
            ICollidable coll2 = new CollisionComponent(r2, CollisionLabel.Label.PLAYER);

            this.collisionSupervisor.AddCollisionComponent(coll1);
            this.collisionSupervisor.AddCollisionComponent(coll2);
            this.result = false;

            coll1.GetEvent.Register(c => {
                this.result = true;
                Assert.AreEqual(c.GetCollisionComponent(), coll2);
            });

            this.collisionSupervisor.SearchCollision();
            this.collisionSupervisor.RemoveCollisionComponent(coll1);
            this.collisionSupervisor.RemoveCollisionComponent(coll2);
            Assert.AreEqual(expected, result);
        }
    }
}
