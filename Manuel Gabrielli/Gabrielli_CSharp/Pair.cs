﻿using System;
using System.Collections.Generic;

namespace Gabrielli_CSharp
{
    public class Pair<X, Y>
    {
        private readonly X fst;
        private readonly Y snd;

        /// <summary>
        /// Constructor for Pair.
        /// </summary>
        /// <param name="fst">First Number</param>
        /// <param name="snd">Second Number</param>
        public Pair(X fst, Y snd) : base()
        {
            this.fst = fst;
            this.snd = snd;
        }

        /// <summary>
        /// Getter for First Number.
        /// </summary>
        /// <returns>First Number</returns>
        public X GetFst()
        {
            return fst;
        }

        /// <summary>
        /// Getter for Second Number.
        /// </summary>
        /// <returns>Second Number</returns>
        public Y GetSnd()
        {
            return snd;
        }

        public override int GetHashCode()
        {
            var hashCode = 1452075630;
            hashCode = hashCode * -1521134295 + EqualityComparer<X>.Default.GetHashCode(fst);
            hashCode = hashCode * -1521134295 + EqualityComparer<Y>.Default.GetHashCode(snd);
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            var pair = obj as Pair<X, Y>;
            return pair != null &&
                   EqualityComparer<X>.Default.Equals(fst, pair.fst) &&
                   EqualityComparer<Y>.Default.Equals(snd, pair.snd);
        }

        public override String ToString()
        {
            return "[" + this.fst + ";" + this.snd + "]";
        }
    }
}
