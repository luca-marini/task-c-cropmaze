﻿using System;
using System.Collections.Generic;

namespace Gabrielli_CSharp
{
    /// <summary>
    /// Class responsible for the score list.
    /// </summary>
    public interface IScoreList
    {
        /// <summary>
        /// Add a new score to the file.
        /// </summary>
        /// <param name="score">The score to insert</param>
        void AddScore(Pair<String, int> score);

        /// <summary>
        /// Getter for the list of the ranking.
        /// </summary>
        /// <returns>List of ranking</returns>
        List<Pair<string, int>> GetRanking();

        /// <summary>
        /// Clears the ranking list.
        /// </summary>
        /// <exception cref="IOException">Exception if the file does not exist</exception>
        void DeleteAll();
    }
}
