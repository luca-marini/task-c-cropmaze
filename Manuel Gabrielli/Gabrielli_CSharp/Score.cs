﻿using System;
using System.Collections.Generic;

namespace Gabrielli_CSharp
{
    [Serializable]
    public class Score
    {
        private static readonly long serialVersionUID = 5044857723462203092L;
        private readonly String playerName;
        private readonly int scoreAchieved;

        /// <summary>
        /// Constructor for Score.
        /// </summary>
        /// <param name="playerName">The name of the player</param>
        /// <param name="scoreAchieved">The score achieved</param
        public Score(string playerName, int scoreAchieved)
        {
            this.playerName = playerName;
            this.scoreAchieved = scoreAchieved;
        }

        /// <summary>
        /// Getter for the player's name.
        /// </summary>
        /// <returns>The name of the player</returns>
        public String GetPlayerName()
        {
            return this.playerName;
        }

        /// <summary>
        /// Getter for the player's score.
        /// </summary>
        /// <returns>The score achieved</returns>
        public int GetScore()
        {
            return this.scoreAchieved;
        }

        public override bool Equals(object obj)
        {
            var score = obj as Score;
            return score != null &&
                   playerName == score.playerName &&
                   scoreAchieved == score.scoreAchieved;
        }

        public override int GetHashCode()
        {
            var hashCode = 961490171;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(playerName);
            hashCode = hashCode * -1521134295 + scoreAchieved.GetHashCode();
            return hashCode;
        }
    }
}