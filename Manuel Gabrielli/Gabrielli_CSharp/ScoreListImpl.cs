﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gabrielli_CSharp
{
    /// <summary>
    /// Implementation of IScoreList.
    /// </summary>
    public class ScoreListImpl : IScoreList
    {
        private static readonly string DIR_PATH = Path.Combine(Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().Location).LocalPath), ".CropMaze");
        private static readonly string FILE_NAME = Path.Combine(DIR_PATH, "Ranking.txt");
        private static readonly int RANKING_SIZE = 5;
        private List<Score> scoreList = new List<Score>();

        /// <summary>
        /// Constructor for ScoreListImpl.
        /// </summary>
        /// <exception cref="IOException">
        /// Exception if the file does not exist.
        /// </exception>
        public ScoreListImpl()
        {
            Initiate();

            try
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Read);
                int size = (int)formatter.Deserialize(stream);
                for (int i = 0; i < size; i++)
                {
                    this.scoreList.Add((Score)formatter.Deserialize(stream));
                }
                stream.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Failed read from file\n");
            }
        }

        public void AddScore(Pair<string, int> score)
        {
            Score newScore = new Score(score.GetFst(), score.GetSnd());
            if (!this.scoreList.Contains(newScore))
            {
                this.scoreList.Add(newScore);
                this.scoreList.Sort((s1, s2) => s2.GetScore() - s1.GetScore());
                if (this.scoreList.Count > RANKING_SIZE)
                {
                    this.scoreList = this.scoreList.GetRange(0, RANKING_SIZE);
                }

                WriteOnFile();
            } //if it contains the exact score it does nothing

        }

        /// <summary>
        /// Method that writes the list on file.
        /// </summary>
        /// <exception cref="IOException">Exception if the file does not exist</exception>
        private void WriteOnFile()
        {
            try
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Write);
                formatter.Serialize(stream, this.scoreList.Count);
                this.scoreList.ForEach(s => formatter.Serialize(stream, s));
                stream.Close();
            }
            catch (IOException e)
            {
                throw new IOException("Failed to write on file\n");
            }
        }

        public void DeleteAll()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Write);
            formatter.Serialize(stream, 0);
            stream.Close();
        }

        public List<Pair<string, int>> GetRanking()
        {
            return this.scoreList.ConvertAll(new Converter<Score, Pair<string, int>>(s => new Pair<string, int>(s.GetPlayerName(), s.GetScore())));
        }

        /// <summary>
        /// Method that creates the rankingFile in the home folder.
        /// </summary>
        /// <exception cref="IOException">
        /// Exception thrown if the file can't be created
        /// </exception>
        private void Initiate()
        {
            if (!Directory.Exists(DIR_PATH))
            {
                try
                {
                    Directory.CreateDirectory(DIR_PATH);
                    File.Create(FILE_NAME).Close();
                    DeleteAll();
                }
                catch (Exception e)
                {
                    throw new IOException("Failed to crate the directory or file\n");
                }
            }
        }
    }
}
