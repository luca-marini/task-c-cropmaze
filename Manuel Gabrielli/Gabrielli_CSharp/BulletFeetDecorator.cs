﻿using System.Drawing;

namespace Gabrielli_CSharp
{
    /// <summary>
    /// Component used to destroy the Bullet after a number of steps.
    /// </summary>
    class BulletFeetDecorator : AbstractMovement
    {
        private static readonly int STEP = 1;
        private readonly Life lifeComponent;
        private readonly Weapon weapon;
        private readonly Direction direction;
        private readonly Feet feet;

        /// <summary>
        /// Constructor for LimitedBulletFeet.
        /// </summary>
        /// <param name="lifeComponent">The weapon of the player</param>
        /// <param name="weapon">The direction of the bullet</param>
        /// <param name="direction">lifeComponent of the bullet</param>
        /// <param name="feet">The feet to decorate</param>
        public BulletFeetDecorator(Life lifeComponent, Weapon weapon, Direction direction, Feet feet) : base()
        {
            this.lifeComponent = lifeComponent;
            this.weapon = weapon;
            this.direction = direction;
            this.feet = feet;
        }

        public override void Move(Point direction)
        {
            if (this.lifeComponent.IsAlive())
            {
                this.feet.Move(direction);
                if (this.feet.GetPosition().Equals(Point.Empty))
                {
                    Remove();
                }
                else
                {
                    this.lifeComponent.Damaged(STEP);
                }
            }
            else
            {
                Remove();
            }
        }

        public override double GetSpeed()
        {
            return this.feet.GetSpeed();
        }

        public override void SetSpeed(double speed)
        {
            this.feet.SetSpeed(speed);
        }

        public override Point CalculateVector(Direction direction)
        {
            return this.feet.CalculateVector(direction);
        }

        public override void Update(double dt)
        {
            Move(CalculateVector(this.direction));
        }

        /// <summary>
        /// Method to remove the bullet.
        /// </summary>
        private void Remove()
        {
            this.weapon.RemoveBullet();
        }
    }
}
