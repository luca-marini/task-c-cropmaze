﻿using System.Drawing;

namespace Gabrielli_CSharp
{
    interface IMovement
    {
        void Move(Point direction);

        void SetSpeed(double speed);

        double GetSpeed();

        Point CalculateVector(Direction direction);

        void Update(double dt);
    }
}
