﻿using System.Drawing;

namespace Gabrielli_CSharp
{
    abstract class AbstractMovement : IMovement
    {
        public abstract Point CalculateVector(Direction direction);

        public abstract double GetSpeed();

        public abstract void Move(Point direction);

        public abstract void SetSpeed(double speed);

        public abstract void Update(double dt);
    }
}
