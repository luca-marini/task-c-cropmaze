﻿using System.Drawing;

namespace Gabrielli_CSharp
{
    class Feet : AbstractMovement
    {
        public override Point CalculateVector(Direction direction)
        {
            return Point.Empty;
        }

        public Point GetPosition()
        {
            return Point.Empty;
        }

        public override double GetSpeed()
        {
            return 0;
        }

        public override void Move(Point direction)
        {
        }

        public override void SetSpeed(double speed)
        {
        }

        public override void Update(double dt)
        {
        }
    }
}
