﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gabrielli_CSharp;


namespace UnitTestProject
{
    /// <summary>
    /// Test for Ranking using.
    /// </summary>
    [TestClass]
    public class TestRanking
    {
        private static readonly List<Pair<string, int>> EMPTY_LIST = new List<Pair<string, int>>();
        private static readonly Pair<string, int> GIOVANNI = new Pair<string, int>("Giovanni", 45);
        private static readonly Pair<string, int> LUCA = new Pair<string, int>("Luca", 20);
        private static readonly Pair<string, int> MARIO = new Pair<string, int>("Mario", 10);
        private static readonly Pair<string, int> ANTONIO = new Pair<string, int>("Antonio", 6);
        private static readonly Pair<string, int> LUCIA = new Pair<string, int>("Lucia", 30);
        private static readonly Pair<string, int> SIMONE = new Pair<string, int>("Simone", 60);

        /// <summary>
        /// Basic test for Ranking writing on file.
        /// </summary>
        [TestMethod]
        public void TestWriteInOrder()
        {
            IScoreList scoreList = new ScoreListImpl();

            //test file is empty
            scoreList.DeleteAll();
            CollectionAssert.AreEqual(EMPTY_LIST, scoreList.GetRanking());

            // Write something to it.
            scoreList.AddScore(GIOVANNI);
            scoreList.AddScore(LUCA);

            List<Pair<string, int>> list = new List<Pair<string, int>>
            {
                GIOVANNI,
                LUCA
            };

            CollectionAssert.AreEqual(list, scoreList.GetRanking());

            //delete all
            scoreList.DeleteAll();
        }

        /// <summary>
        /// Test for Ranking writing not in order.
        /// </summary>
        [TestMethod]
        public void TestWriteNotInOrder()
        {
            IScoreList scoreList = new ScoreListImpl();

            //test file is empty
            scoreList.DeleteAll();
            CollectionAssert.AreEqual(EMPTY_LIST, scoreList.GetRanking());

            // Write something to it.
            scoreList.AddScore(LUCA);
            scoreList.AddScore(MARIO);
            scoreList.AddScore(GIOVANNI);

            List<Pair<string, int>> list = new List<Pair<string, int>>
            {
                GIOVANNI,
                LUCA,
                MARIO
            };

            CollectionAssert.AreEqual(list, scoreList.GetRanking());

            //delete all
            scoreList.DeleteAll();
        }

        /// <summary>
        /// Test for Ranking writing more than max.
        /// </summary>
        [TestMethod]
        public void TestWriteMoreThanMax()
        {
            IScoreList scoreList = new ScoreListImpl();

            //test file is empty
            scoreList.DeleteAll();
            CollectionAssert.AreEqual(EMPTY_LIST, scoreList.GetRanking());

            // Write something to it.
            scoreList.AddScore(LUCA);
            scoreList.AddScore(MARIO);
            scoreList.AddScore(GIOVANNI);
            scoreList.AddScore(ANTONIO);
            scoreList.AddScore(LUCIA);
            scoreList.AddScore(SIMONE);

            List<Pair<string, int>> list = new List<Pair<string, int>>
            {
                SIMONE,
                GIOVANNI,
                LUCIA,
                LUCA,
                MARIO
            };

            CollectionAssert.AreEqual(list, scoreList.GetRanking());

            //delete all
            scoreList.DeleteAll();
        }

        /// <summary>
        /// Test for Ranking writing with repetitions.
        /// </summary>
        [TestMethod]
        public void TestWriteSameScore()
        {
            IScoreList scoreList = new ScoreListImpl();

            //test file is empty
            scoreList.DeleteAll();
            CollectionAssert.AreEqual(EMPTY_LIST, scoreList.GetRanking());

            // Write something to it.
            scoreList.AddScore(LUCA);
            scoreList.AddScore(MARIO);
            scoreList.AddScore(GIOVANNI);
            scoreList.AddScore(GIOVANNI);
            scoreList.AddScore(LUCA);

            List<Pair<string, int>> list = new List<Pair<string, int>>
            {
                GIOVANNI,
                LUCA,
                MARIO
            };

            CollectionAssert.AreEqual(list, scoreList.GetRanking());

            //delete all
            scoreList.DeleteAll();
        }
    }
}
