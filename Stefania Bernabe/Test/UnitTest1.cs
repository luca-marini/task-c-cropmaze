﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Collections.Generic;
using progetto_bernabe;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        private static readonly int INITIAL_POSITION_X = 20;
        private static readonly int INITIAL_POSITION_Y = 20;
        private Point INITIAL_FEET_POSITION = new Point(INITIAL_POSITION_X, INITIAL_POSITION_Y);
        private Size DIMENSION = new Size(1, 1);
        private static ISet<Wall> walls = new HashSet<Wall>();
        private static readonly int walk_speed = 1;
        private static readonly Direction INITIAL_DIRECTION = Direction.NOTHING;
        private Feet feet;

        public void InitializeFeet(ISet<Wall> walls)
        {
            feet = new Feet(walk_speed, walls, INITIAL_FEET_POSITION, INITIAL_DIRECTION);
        }

        [TestMethod]
        public void TestInitPosition()
        {
            InitializeFeet(null);
            Assert.AreEqual(feet.GetPosition(), new Point(INITIAL_POSITION_X, INITIAL_POSITION_Y));
        }

        [TestMethod]
        public void TestMoveWithoutWallsOnTheWay()
        {
            Wall wall = new Wall(new Point(0, 0), new Size(1, 1));
            
            walls.Add(wall);
            InitializeFeet(walls);
            //MOVE TO NORTH OF 4 POSITION
            for (int i = 0; i < 4; i++)
            {
                feet.Move(new Point(0, 1));
            }       
            Assert.AreEqual(feet.GetPosition(), new Point(INITIAL_POSITION_X, INITIAL_POSITION_Y + 4));
            
            //MOVE TO EAST OF 3 POSITION
            for(int i = 0; i < 3; i++)
            {
                feet.Move(new Point(1, 0));
            }
            Assert.AreEqual(feet.GetPosition(), new Point(INITIAL_POSITION_X + 3, INITIAL_POSITION_Y + 4));

        }

        [TestMethod]
        public void TestMoveWithWallOnTheWay()
        {
            Wall wall = new Wall(new Point(20, 21), new Size(1, 1));
            walls.Add(wall);
            InitializeFeet(walls);
            //MOVE TO NORTH OF 4 POSITION BUT IT WILL FIND A WALL AND IT WILL BE IN POSITION {20,23} INSTEAD OF {20,24}
            for (int i = 0; i < 4; i++)
            {
                feet.Move(new Point(0, 1));
            }
            Assert.AreNotEqual(feet.GetPosition(), new Point(INITIAL_POSITION_X, INITIAL_POSITION_Y + 4));

        }


    }
}
