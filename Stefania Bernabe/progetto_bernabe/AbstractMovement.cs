﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace progetto_bernabe
{
    public abstract class AbstractMovement: AbstractEntityComponent, IMovement
    {
        private State currentState = State.STABLE;
        private Point desiredPosition = Point.Empty;
        private Direction direction = Direction.NOTHING;

        

        public State GetState()
        {
            return currentState;
        }

        public void Update(double dt)
        {
        }

        protected void SetState(State newState)
        {
            if (!currentState.Equals(newState))
            {
                currentState = newState;
            }
        }

        public Point GetPosition()
        {
            return desiredPosition;
        }

        protected void SetPosition(Point newMovement)
        {
            this.desiredPosition = newMovement;
        }

        protected Direction GetDirection()
        {
            return this.direction;
        }

        protected void SetDirection(Direction newDirection)
        {
            this.direction = newDirection;
        }

        public abstract void Move(Point direction);
        public abstract void SetSpeed(int newSpeed);
        public abstract Point CalculateVector(Direction direction);
        public abstract int GetSpeed();
    }
}
