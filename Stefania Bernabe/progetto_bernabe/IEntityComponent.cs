﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progetto_bernabe
{
    public interface IEntityComponent
    {
        void Attach(IEntity owner);

        void Detach();

        IEntity GetOwner();
    }
}
