﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progetto_bernabe
{
    public abstract class AbstractEntity : IEntity
    {
        private readonly IEntityBody body;

        public AbstractEntity( IEntityBody body)
        {
            this.body = body;
            body.Attach(this);
        }

        public void Add(IEntityComponent component)
        {
            throw new NotImplementedException();
        }

        public void Destroy()
        {
            this.Remove(body);
        }

        public IEntityComponent Get(IEntityComponent component)
        {
            throw new NotImplementedException();
        }

        public IEntityBody GetBody()
        {
            return body;
        }

        public void Remove(IEntityComponent component)
        {
            throw new NotImplementedException();
        }

        public void RemoveEntity()
        {
            throw new NotImplementedException();
        }

        public void Update(double up)
        {
        }
    }
}
