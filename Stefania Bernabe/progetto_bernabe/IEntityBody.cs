﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace progetto_bernabe
{
    public interface IEntityBody : IEntityComponent
    {
        void AddPosition(Point position);

        void ChangeDirection(Direction direction);

        Point GetPosition();


    }
}
