﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace progetto_bernabe
{
    public interface IMovement : IEntityComponent
    {
        void Move(Point direction);

        State GetState();

        void Update(double dt);

        int GetSpeed();

        void SetSpeed(int newSpeed);

        Point CalculateVector(Direction direction);


    }
}
