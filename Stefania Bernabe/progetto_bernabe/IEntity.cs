﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progetto_bernabe
{
    public interface IEntity
    {
        IEntityBody GetBody();
        IEntityComponent Get(IEntityComponent component);

        void Remove(IEntityComponent component);

        void Add(IEntityComponent component);

        void Update(double up);

        void Destroy();

        void RemoveEntity();
    }
}
