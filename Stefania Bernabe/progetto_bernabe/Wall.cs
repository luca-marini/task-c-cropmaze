﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace progetto_bernabe
{
    public class Wall
    {
        private Point wallPosition;
        private Size wallSize;
        public Wall(Point position, Size size)
        {
            this.wallPosition = position;
            this.wallSize = size;
        }

        public Size GetDimension()
        {
            return this.wallSize;
        }

        public Rectangle GetShape()
        {
            return new Rectangle(wallPosition.X, wallPosition.Y, 1, 1);
        }

        public Point GetPosition()
        {
            return this.wallPosition;
        }
    }
}
