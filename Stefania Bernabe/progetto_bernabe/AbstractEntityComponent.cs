﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progetto_bernabe
{
    public abstract class AbstractEntityComponent : IEntityComponent
    {
        private IEntity owner;
        public void Attach(IEntity owner)
        {
            if (this.owner == null)
            {
                this.owner = owner;
            }
        }

        public void Detach()
        {
            this.owner = null;
        }

        public IEntity GetOwner()
        {
            return this.owner;
        }
    }
}
