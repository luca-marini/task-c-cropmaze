﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using progetto_bernabe;

namespace progetto_bernabe
{
    public class Feet : AbstractMovement
    {
        private int walkingSpeed;
        private readonly ISet<Wall> walls;
        //private Point feetPosition;
        //private Direction feetDirection;

        public Feet(int walkingSpeed, ISet<Wall> walls, Point feetPosition, Direction feetDirection)
            : base()
        {
            this.walls = walls;
            this.walkingSpeed = walkingSpeed;
            this.SetPosition(feetPosition);
            this.SetDirection(feetDirection);
        }

        private Boolean WallChecker(Point distanceVector)
        {
            int fl = 0;
            Rectangle shape = new Rectangle(distanceVector.X, distanceVector.Y, 1, 1);

            
            if (walls.Any())
            {
                foreach (Wall w in walls)
                {
                    Console.WriteLine("wall position" + w.GetPosition());

                    if (shape.IntersectsWith(w.GetShape()))
                    {
                        Console.WriteLine("find a wall");
                        fl = 1;
                    }
                }
            }

            if (fl == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        
    }


    public Point CalculateNewDistanceVector(Point distanceVector)
    {
        int hf = distanceVector.X > 0 ? 1 : distanceVector.X < 0 ? -1 : 0;
        int vf = distanceVector.Y > 0 ? 1 : distanceVector.Y < 0 ? -1 : 0;

        return new Point(hf * walkingSpeed, vf * walkingSpeed);
    }

    public Direction CalculateNewDirection(Point vector)
    {
            Direction newDirection = Direction.NOTHING;
            if (vector.Y != vector.X)
            {
                if (vector.X == 0 && vector.Y > 0)
                {
                    newDirection = Direction.NORTH;
                }

                else if (vector.X == 0 && vector.Y < 0)
                {
                    newDirection = Direction.SOUTH;
                }

                else if (vector.X < 0 && vector.Y == 0)
                {
                    newDirection = Direction.WEST;
                }

                else if(vector.X > 0 && vector.Y == 0)
                {
                    newDirection = Direction.EAST;
                }
            }

            return newDirection;
    }

    private void UpdateState()
    {
        if (this.GetPosition().Equals(Point.Empty))
        {
            SetState(State.WALKINING);
        }
        else
        {
            this.SetDirection(Direction.NOTHING);
            SetState(State.STABLE);
        }
    }

    public override void Move(Point distanceVector)
    {
        Point movementVector = CalculateNewDistanceVector(distanceVector);
        Point temp = new Point(movementVector.X, movementVector.Y);
        Point newPos = new Point(temp.X + this.GetPosition().X, temp.Y + this.GetPosition().Y);

        if (WallChecker(newPos))
        {
            SetPosition(Point.Empty);
            Console.WriteLine("find a wall");
        }
        else
        {
            this.SetPosition(newPos);
            this.SetDirection(CalculateNewDirection(movementVector));
        }

        Console.WriteLine("moving to " + this.GetDirection() + " in position " + this.GetPosition());
        UpdateState();
    }

    public override void SetSpeed(int newSpeed)
    {
        this.walkingSpeed = newSpeed;
    }

    public override Point CalculateVector(Direction direction)
    {
        return new Point(0, 0);
    }

    public override int GetSpeed()
    {
        return this.walkingSpeed;
    }
}
}
